import path from 'path';
import { readFileSync } from 'fs';
import CartParser from './CartParser.js';

let parser, parse, validate, parseLine, calcTotal;

beforeEach(() => {
	parser = new CartParser();
	parse = parser.parse.bind(parser);
	validate = parser.validate.bind(parser);
	parseLine = parser.parseLine.bind(parser);
	calcTotal = parser.calcTotal;
});

describe('CartParser - unit tests', () => {
	describe('validate', () => {
		it('should return non empty array if column header name is wrong', () => {
			const content = `Product name,Price1,Quantity`;
			const errorExpected = {
				column: 1,
				message: `Expected header to be named "Price" but received Price1.`,
				row: 0,
				type: "header"
			};
			expect(validate(content)).toEqual(
				expect.arrayContaining([errorExpected])
			);
		});
		it('should return non empty array if columns count in row is less than needed', () => {
			const content = `Product name,Price,Quantity
											 a,1`;
			const errorExpected = {
				column: -1,
				message: `Expected row to have 3 cells but received 2.`,
				row: 1,
				type: "row"
			};
			expect(validate(content)).toEqual(
				expect.arrayContaining([errorExpected])
			);
		});
		it('should return non empty array if first column in row is empty', () => {
			const content = `Product name,Price,Quantity
											 ,1,1`;
			const errorExpected = {
				column: 0,
				message: `Expected cell to be a nonempty string but received "".`,
				row: 1,
				type: "cell"
			};
			expect(validate(content)).toEqual(
				expect.arrayContaining([errorExpected])
			);
		});
		it('should return non empty array if non-first column in row is not a positive number', () => {
			let content = `Product name,Price,Quantity
										 a,a,a`;
			let errorExpected = {
				column: 1,
				message: `Expected cell to be a positive number but received "a".`,
				row: 1,
				type: "cell"
			};
			expect(validate(content)).toEqual(
				expect.arrayContaining([errorExpected])
			);

			content = `Product name,Price,Quantity
											 a,-1,-1`;
			errorExpected = {
				column: 1,
				message: `Expected cell to be a positive number but received "-1".`,
				row: 1,
				type: "cell"
			};
			expect(validate(content)).toEqual(
				expect.arrayContaining([errorExpected])
			);

			content = `Product name,Price,Quantity
											 a,NaN,-1`;
			errorExpected = {
				column: 1,
				message: `Expected cell to be a positive number but received "NaN".`,
				row: 1,
				type: "cell"
			};
			expect(validate(content)).toEqual(
				expect.arrayContaining([errorExpected])
			);
		});
		it('should return empty array if input is valid', () => {
			const content = `Product name,Price,Quantity
											 a,1,2`;
			expect(validate(content)).toEqual([]);
		});
	});
	describe('parseLine', () => {
		it('should return valid object from valid string', () => {
			const content = `a,1,2`;
			const resultExpected = {
				id: expect.any(String),
				name: 'a',
				price: 1,
				quantity: 2
			}
			expect(parseLine(content)).toStrictEqual(resultExpected);
		});
	});
	describe('calcTotal', () => {
		it('should return number from array of row objects', () => {
			let content = [
				{
					price: 1,
					quantity: 1
				},
				{
					price: 2,
					quantity: 2
				}
			]
			let resultExpected = 5;
			expect(calcTotal(content)).toBe(resultExpected);

			content = [
				{
					price: 1,
					quantity: 0
				},
				{
					price: 0,
					quantity: 20
				}
			]
			resultExpected = 0;
			expect(calcTotal(content)).toBe(resultExpected);
		});
	});
	describe('parse', () => {
		it('should call readFile', () => {
			const content = 'file-path';
			const spy = jest.spyOn(parser, 'readFile').mockImplementation(() => content);
			expect(parser.readFile()).toBe(content);
			expect(spy).toHaveBeenCalledTimes(1);
		})
		it('should throw error if file content is not valid', () => {
			expect(() => {
				parse('');
			}).toThrow();
		});
		it('should return valid object if file content is valid', () => {
			const content = `Product name,Price,Quantity
											 a,1,1`;
			parser.readFile = () => content;

			const resultExpected = {
				items: [
					{
						id: expect.any(String),
						name: 'a',
						price: 1,
						quantity: 1
					}
				],
				total: 1
			}
			expect(parse('')).toStrictEqual(resultExpected);
		});
	});
});

describe('CartParser - integration test', () => {
	it('should process CSV file to match JSON file', () => {
		parser = new CartParser();
		const csvPath = path.join(__dirname, '..', 'samples', 'cart.csv');
		const resultActual = parser.parse(csvPath);
		resultActual.items = resultActual.items.map(item => ({
			...item,
			id: 'id'
		}));
		const jsonPath = path.join(__dirname, '..', 'samples', 'cart.json');
		const resultExpected = JSON.parse(readFileSync(jsonPath, 'utf-8', 'r'));
		resultExpected.items = resultExpected.items.map(item => ({
			...item,
			id: 'id'
		}));

		expect(resultExpected).toStrictEqual(resultActual);
	});
});